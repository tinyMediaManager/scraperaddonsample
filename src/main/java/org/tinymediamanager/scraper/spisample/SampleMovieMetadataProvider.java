package org.tinymediamanager.scraper.spisample;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ResourceBundle;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tinymediamanager.core.entities.MediaRating;
import org.tinymediamanager.core.movie.MovieSearchAndScrapeOptions;
import org.tinymediamanager.scraper.MediaMetadata;
import org.tinymediamanager.scraper.MediaProviderInfo;
import org.tinymediamanager.scraper.MediaSearchResult;
import org.tinymediamanager.scraper.entities.MediaCertification;
import org.tinymediamanager.scraper.entities.MediaType;
import org.tinymediamanager.scraper.exceptions.ScrapeException;
import org.tinymediamanager.scraper.http.InMemoryCachedUrl;
import org.tinymediamanager.scraper.http.Url;
import org.tinymediamanager.scraper.interfaces.IMovieMetadataProvider;
import org.tinymediamanager.scraper.util.MediaIdUtil;
import org.tinymediamanager.scraper.util.MetadataUtil;
import org.tinymediamanager.scraper.util.StrgUtils;
import org.tinymediamanager.scraper.util.UrlUtil;

public class SampleMovieMetadataProvider implements IMovieMetadataProvider {

  private static final Logger     LOGGER = LoggerFactory.getLogger(SampleMovieMetadataProvider.class);
  private final MediaProviderInfo providerInfo;

  public SampleMovieMetadataProvider() {
    providerInfo = createProviderInfo();
  }

  private MediaProviderInfo createProviderInfo() {
    MediaProviderInfo info = new MediaProviderInfo("spi-sample", "movie", "SPI Sample", "A sample for a dynamic movie metadata scraper",
        SampleMovieMetadataProvider.class.getResource("/org/tinymediamanager/scraper/spisample/tmm_logo.svg"));
    // the ResourceBundle to offer i18n support for scraper options
    info.setResourceBundle(ResourceBundle.getBundle("org.tinymediamanager.scraper.spisample.messages"));

    // create configuration properties
    info.getConfig().addText("text", "", false);
    info.getConfig().addBoolean("boolean", true);
    info.getConfig().addInteger("integer", 10);
    info.getConfig().addSelect("select", new String[] { "A", "B", "C" }, "A");

    // load any existing values from the storage
    info.getConfig().load();

    return info;
  }

  @Override
  public MediaProviderInfo getProviderInfo() {
    return providerInfo;
  }

  @Override
  public boolean isActive() {
    return true;
  }

  @Override
  public SortedSet<MediaSearchResult> search(MovieSearchAndScrapeOptions options) throws ScrapeException {
    SortedSet<MediaSearchResult> results = new TreeSet<>();

    String searchTerm = "";
    if (MediaIdUtil.isValidImdbId(options.getImdbId())) {
      searchTerm = options.getImdbId();
    }
    if (searchTerm.isBlank()) {
      searchTerm = options.getSearchQuery();
    }
    if (searchTerm.isBlank()) {
      return results;
    }

    // parse out language and country from the scraper query - for passing to the scrape
    String language = options.getLanguage().getLanguage();
    String country = options.getCertificationCountry().getAlpha2();

    searchTerm = MetadataUtil.removeNonSearchCharacters(searchTerm);

    ////////////////////////////// DO SOME SEARCHING HERE, AND ADD ALL RESULTS TO TREESET
    try {
      Url search = new InMemoryCachedUrl("https://www.google.com/?q=" + URLEncoder.encode(searchTerm, StandardCharsets.UTF_8));
      InputStream is = search.getInputStream();
      Document doc = Jsoup.parse(is, UrlUtil.UTF_8, "");

    }
    catch (MalformedURLException e) {
      throw new ScrapeException(e.getMessage());
    }
    catch (IOException e) {
      throw new ScrapeException(e.getMessage());
    }
    catch (InterruptedException e) {
      // may be fine if user clicks abort
      LOGGER.warn("Scrape aborted.");
    }

    // FOREACH RESULT DO
    MediaSearchResult result = new MediaSearchResult(getId(), MediaType.MOVIE);

    // set the values of the search result.
    // id, title, year and score is needed
    result.setId(getId());
    result.setTitle("This is a result");
    result.setYear(2021);

    // set the search score. should be calculated from the search query
    // you can use the helper either
    result.calculateScore(options);
    // or set the score directly (0...1)
    result.setScore(1.0f);

    results.add(result);
    // FOREACH RESULT END

    // more detailed example can be found in the tmm repo:
    // https://gitlab.com/tinyMediaManager/tinyMediaManager/-/tree/devel/src/main/java/org/tinymediamanager/scraper

    return results;
  }

  @Override
  public MediaMetadata getMetadata(MovieSearchAndScrapeOptions movieSearchAndScrapeOptions) throws ScrapeException {
    MediaMetadata mediaMetadata = new MediaMetadata(getId());

    // here you may set all available data
    mediaMetadata.setTitle("title");
    mediaMetadata.setOriginalTitle("original title");
    mediaMetadata.setPlot("the plot of the movie");
    mediaMetadata.setYear(2021);
    try {
      mediaMetadata.setReleaseDate(StrgUtils.parseDate("2010-06-30"));
    }
    catch (ParseException e) {
      LOGGER.warn("Could not parse date: {}", e.getMessage());
    }
    mediaMetadata.setId(getId(), 1234);
    mediaMetadata.setId(MediaMetadata.IMDB, "tt123456");
    mediaMetadata.addRating(new MediaRating(getId(), 5.5f, 200, 10));
    mediaMetadata.addRating(new MediaRating(MediaMetadata.IMDB, 7.8f, 20000));
    mediaMetadata.addCertification(MediaCertification.US_G);
    mediaMetadata.addCertification(MediaCertification.DE_FSK12);

    // more detailed example can be found in the tmm repo:
    // https://gitlab.com/tinyMediaManager/tinyMediaManager/-/tree/devel/src/main/java/org/tinymediamanager/scraper

    return mediaMetadata;
  }
}
